const api = "http://localhost:8080"

async function json(response) {
  return await response.json()
}

export async function convert(file) {
  const { file_id: fileId } = await fetch(`${api}/api/convert/initiate`, { method: "POST" }).then(json)

  await fetch(`${api}/api/convert/${fileId}/chunks`, {
    method: "PUT",
    body: file
  })

  const { lines } = await fetch(`${api}/api/convert/${fileId}/complete`, { method: "POST" }).then(json)

  return lines
}
